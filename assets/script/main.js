window.addEventListener('load', () => {
  const hasSubitems = document.querySelectorAll('.has-subitem');

  hasSubitems[1].addEventListener('click', () => {
    hasSubitems[1].classList.toggle('show');
    hasSubitems[1].children[1].classList.toggle('show');
  }, false);

  hasSubitems[0].addEventListener('click', () => {
    hasSubitems[0].classList.toggle('show');
    hasSubitems[0].children[1].classList.toggle('show');
  }, false);

  const mobileBtn = document.querySelector('.mobile__button');

  mobileBtn.addEventListener('click', () => {
    document.querySelector('.mobile__nav')
      .classList.toggle('show');
  }, false);
}, false);
